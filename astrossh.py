#SSH through pluto to configured workstations
#requires paramiko 1.15.1
#pip install paramiko==1.15.1
import paramiko
import getpass
import subprocess
import os


def astrossh():
	config = paramiko.SSHConfig()
	ssh = paramiko.SSHClient()
	ssh.load_host_keys('/Users/hendel/.ssh/known_hosts')
	server = 'amalthea.astro.columbia.edu'
	username = 'hendel'
	config.parse(open(os.path.expanduser('~/.ssh/config')))
	host = config.lookup(server)
	if 'proxycommand' in host:
		proxy = paramiko.ProxyCommand(
			subprocess.check_output([os.environ['SHELL'], '-c', 'echo %s' % host['proxycommand']]).strip()
		)
	else:
		proxy = None
	ssh.connect(host['hostname'], username=username, sock=proxy)
	sftp = ssh.open_sftp()

	return ssh, sftp